package com.sortingapp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingValueExpectedBehaviorTest {
    private final SortingValues sortingValues = new SortingValues();
    private final int[] inputNumbers;
    private final int[] expected;

    public SortingValueExpectedBehaviorTest(int[] inputNumbers, int[] expected) {
        this.inputNumbers = inputNumbers;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> parameters() {
        return Arrays.asList(new int[][][]{
                {{4, 3, 2, 1}, {1, 2, 3, 4}},
                {{1, 2, 3, 4, 5}, {1, 2, 3, 4, 5}},
                {{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
                {{1, 1, 1, 0, 0}, {0, 0, 1, 1, 1}}
        });
    }

    @Test
    public void testValidElementsSorting() {
        assertArrayEquals(expected, sortingValues.sort(inputNumbers));
    }
}
