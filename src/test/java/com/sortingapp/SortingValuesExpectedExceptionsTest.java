package com.sortingapp;

import org.junit.Test;

public class SortingValuesExpectedExceptionsTest {

    private final SortingValues sortingValues = new SortingValues();

    @Test(expected = RuntimeException.class)
    public void testZeroElements_Sorting() {
        int[] inputNumbers = new int[]{};
        sortingValues.sort(inputNumbers);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAboveTenAndBelow99_Sorting() {
        int[] inputNumbers = new int[]{11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        sortingValues.sort(inputNumbers);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testAbove100_Sorting() {
        int[] inputNumbers = new int[100];
        for(int i = 0; i < inputNumbers.length - 1; i++) {
            inputNumbers[i] = 1;
        }
        sortingValues.sort(inputNumbers);
    }
}
