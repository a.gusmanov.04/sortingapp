package com.sortingapp;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({SortingValueExpectedBehaviorTest.class, SortingValuesExpectedExceptionsTest.class})
public class SortingValuesTestSuit {

}
