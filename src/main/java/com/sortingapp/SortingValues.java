package com.sortingapp;

public class SortingValues {

    /**
     * @param array - unsorted array, which size is needed to be between [1-10]
     * @return sorted array in ascending order
     */
    public int[] sort(int[] array) {
        if (array.length == 0) {
            throw new RuntimeException("The length can not be 0");
        }
        if (array.length > 10 && array.length < 99) {
            throw new IllegalArgumentException("Only 10 elements can be inputted");
        }
        if (array.length > 99) {
            throw new UnsupportedOperationException("Invalid number of elements");
        }
        for(int i = 0; i < array.length - 1; i++) {
            for(int j = 0; j < array.length - 1 - i; j++) {
                if(array[j] > array[j + 1]) {
                    int swap = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = swap;
                }
            }
        }
        return array;
    }

}
